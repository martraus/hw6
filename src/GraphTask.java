import java.util.*;
import java.util.ArrayList;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("Printout of the graph G");
      g.createRandomSimpleGraph (6, 9);
      g.createRandomArcLenghts();
      System.out.println (g);

      //Graph f = new Graph ("F");
      //f.createRandomSimpleGraph (1000, 1500);
      //f.createRandomArcLenghts();

      // Experiments here
      //g.shortestPathsFrom(g.first);
      Vertex v1 = g.first;
      Vertex v2 = g.first.next.next.next;
      //v1 = g.first; v2 = g.first.next.next.next; //1st test: 4th vertex
      //v1 = g.first; v2 = g.first.next; //2nd test: next vertex
      //v1 = g.first; v2 = g.first; //3rd test: same vertex
      //v1 = g.first, v2 = null; //4th test: null vertex
      //v1 = g.first.next.next.next.next.next; v2 = g.first.next.next; //5th test: source vertex some other then first vertex of the graph
      //v1 = f.first; v2 = f.first.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next.next; //6th test: long graph
      //v1 = f.first; v2 = g.first; //7th test: vertex from other graph

      ArrayList<Arc> shortestArcs = g.shortestPath(v1, v2);

      int shortestLength = v2.getVInfo();
      System.out.println("Shortest length from vertex " + v1 + " to vertex " + v2 + " is " + shortestLength + ".");
      System.out.println("Shortest path is through these edges: ");
      for (int i = shortestArcs.size()-1; i >= 0; i--) {
         System.out.println(shortestArcs.get(i) + "; ");
      }
   }

   /**
    * TASK: Koostada meetod, mis leiab etteantud sidusas lihtgraafis kahe etteantud tipu vahelise lühima tee.
    * @author Jaanus Pöial
    * @author Mart Raus
    * @version 1.0
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // Added fields
      private Object lastClosestVertex = null;

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Vertex methods here!
      public int getVInfo() {
         return info;
      }

      public Object getVLastClosestVertex() {
         return lastClosestVertex;
      }

      public void setVInfo(int info) {
         this.info = info;
      }

      public void setVLastClosestVertex(Object lastClosestVertex) {
         this.lastClosestVertex = lastClosestVertex;
      }

      /**
       * Finds all edges that directly connect with the vertex.
       * @return list of edges
       */
      public List<Arc> allEdges (){
         List<Arc> edges = new ArrayList<>();
         for (Arc a = first; a != null; a = a.next)
            edges.add(a);
         return edges;
      }

      /**
       * Finds the edge for a give destination vertex.
       * @param dest destination vertex
       * @return result correct edge or null if the correct edge was not found
       */
      public Arc findEdge (Vertex dest){
         List<Arc> edges = this.allEdges();
         for (Arc edge : edges)
            if (edge.target == dest) {
            return edge;
            }
         return null;
      }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Arc methods here!
      public int getAInfo() {
         return info;
      }

      public void setInfo(int info) {
         this.info = info;
      }

      public Vertex getTarget() {
         return target;
      }
   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph(String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph(String s) {
         this(s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty("line.separator");
         StringBuffer sb = new StringBuffer(nl);
         sb.append(id);
         sb.append(nl);
         Vertex v = first;
         while (v != null) {
            sb.append(v.toString());
            sb.append(" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append(" ");
               sb.append(a.toString());
               sb.append(" (");
               sb.append(v.toString());
               sb.append("->");
               sb.append(a.target.toString());
               sb.append(", ");
               sb.append(a.getAInfo());
               sb.append(")");
               a = a.next;
            }
            sb.append(nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex(String vid) {
         Vertex res = new Vertex(vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc(String aid, Vertex from, Vertex to) {
         Arc res = new Arc(aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       *
       * @param n number of vertices added to this graph
       */
      public void createRandomTree(int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex[n];
         for (int i = 0; i < n; i++) {
            varray[i] = createVertex("v" + String.valueOf(n - i));
            if (i > 0) {
               int vnr = (int) (Math.random() * i);
               createArc("a" + varray[vnr].toString() + "_"
                       + varray[i].toString(), varray[vnr], varray[i]);
               createArc("a" + varray[i].toString() + "_"
                       + varray[vnr].toString(), varray[i], varray[vnr]);
            } else {
            }
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       *
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int[info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res[i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       *
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph(int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException("Too many vertices: " + n);
         if (m < n - 1 || m > n * (n - 1) / 2)
            throw new IllegalArgumentException
                    ("Impossible number of edges: " + m);
         first = null;
         createRandomTree(n);       // n-1 edges created here
         Vertex[] vert = new Vertex[n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int) (Math.random() * n);  // random source
            int j = (int) (Math.random() * n);  // random target
            if (i == j)
               continue;  // no loops
            if (connected[i][j] != 0 || connected[j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert[i];
            Vertex vj = vert[j];
            createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected[i][j] = 1;
            createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected[j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      // Added Graph methods
      /**
       * Sets random numbers from 0 to 100 to arches info attribute (lengths) in a given graph.
       */
      public void createRandomArcLenghts() {
         for (Vertex v = first; v != null; v = v.next) {
            for (Arc a = v.first; a != null; a = a.next) {
               a.setInfo((int) (Math.random() * 100));
            }
         }
      }

      /**
       * Get all vertexes of a given graph.
       * @return list of vertexes
       * @link http://stackoverflow.com/questions/13627151/count-all-vertices-in-a-graph
       */
      public List<Vertex> getVertexList() {
         List<Vertex> vertices = new ArrayList<>();
         for (Vertex v = first; v != null; v = v.next)
            vertices.add(v);
         return vertices;
      }

      /**
       * Finds and prints out the shortest path of the two vertexes.
       * Side effect: corrupts info fields in the graph
       * @param s1 - source vertex
       * @parem s2 - destination vertex
       */
      public ArrayList<Arc> shortestPath(Vertex s1, Vertex s2) {
         if (s1 == s2)
            throw new RuntimeException("Source and destination vertexes are the same!");
         if ((!getVertexList().contains(s1)))
            throw new RuntimeException("Source vertex is not part of the graph");
         if ((!getVertexList().contains(s2)))
            throw new RuntimeException("Destination vertex is not part of the graph");

         shortestPathsFrom(s1);

         ArrayList<Arc> arcList = new ArrayList<Arc>();
         Vertex v2 = s2;
         Vertex v1 = (Vertex) s2.getVLastClosestVertex();
         while (v1 != null) {
            arcList.add(v2.findEdge((Vertex) v1));
            v2 = v1;
            v1 = (Vertex) v1.getVLastClosestVertex();
         }
         return arcList;
      }

      /**
       * Finds shortest paths from a given vertex. Uses Dijkstra's algorithm.
       * For each vertex vInfo is the length of the shortest path from given
       * source s and vLastClosestVertex is previous closest vertex from s
       * to this vertex.
       * Side effect: corrupts info fields in the graph
       * @param s source vertex
       * @link http://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
       */
      public void shortestPathsFrom(Vertex s) {
         if (getVertexList() == null) return;
         if ((!getVertexList().contains(s)))
            throw new RuntimeException("Source vertex is not part of the graph");
         int INFINITY = Integer.MAX_VALUE / 4;
         Iterator vit = getVertexList().iterator();
         while (vit.hasNext()) {
            Vertex v = (Vertex) vit.next();
            v.setVInfo(INFINITY);
            v.setVLastClosestVertex(null);
         }
         s.setVInfo(0);
         List vq = Collections.synchronizedList(new LinkedList());
         vq.add(s);
         while (vq.size() > 0) {
            int minLen = INFINITY;
            Vertex minVert = null;
            Iterator it = vq.iterator();
            while (it.hasNext()) {
               Vertex v = (Vertex) it.next();
               if (v.getVInfo() < minLen) {
                  minVert = v;
                  minLen = v.getVInfo();
               }
            }
            if (minVert == null)
               throw new RuntimeException("error in Dijkstra!!");
            if (vq.remove(minVert)) {
               // minimal element removed from vq
            } else
               throw new RuntimeException("error in Dijkstra!!");
            it = minVert.allEdges().iterator();
            while (it.hasNext()) {
               Arc a = (Arc) it.next();
               int newLen = minLen + a.getAInfo();
               Vertex to = a.getTarget();
               if (to.getVInfo() == INFINITY) {
                  vq.add(to);
               }
               if (newLen < to.getVInfo()) {
                  to.setVInfo(newLen);
                  to.setVLastClosestVertex(minVert);
               }
            }
         }
      }
   }
} 

